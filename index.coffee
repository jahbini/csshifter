window.onload = ->
  styles = Array::filter.call document.querySelectorAll('link, style'), (node) ->
    node.getAttribute('type') == 'text/csshift'
  shifts = window.colorTransforms

  parseSrc = (src, xhr) ->
    if xhr and xhr.readyState == 4 or !xhr
      for shift,replacer of shifts      
        src = src.replace RegExp(shift,'g'), replacer
      
      style = document.createElement('style')
      style.type = 'text/css'
      if style.styleSheet
        style.styleSheet.cssText = src 
      else
        style.appendChild document.createTextNode(src)
      document.body.appendChild style
    return

  for style in styles
    if style.href
      x = new XMLHttpRequest

      x.onreadystatechange = ->
        parseSrc x.responseText, x
        return

      x.open 'GET', style.href, true
      x.send()
    else
      parseSrc style.innerHTML
  return
